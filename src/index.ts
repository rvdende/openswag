import { SwaggerParser } from "./swaggerparser";
import { IProcessArgs } from './interfaces';
require('source-map-support').install();

if (process.argv.length > 2) {
    console.log(`OpenSwag by Rouan van der Ende. https://www.npmjs.com/package/openswag`)
    let options: IProcessArgs = {}
    // url
    if (process.argv[2].includes('://')) {
        options.url = process.argv[2];
    }
    // file
    if (!process.argv[2].includes('://')) {
        options.file = process.argv[2];
    }

    console.log(options);
    
    const swaggerParser = new SwaggerParser();
    swaggerParser.process(options);
}

process.on('unhandledRejection', console.log);