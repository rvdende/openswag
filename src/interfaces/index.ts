export interface IProcessArgs {
    url?: string
    file?: string
}

import { OpenApi, OpenApiReference, OpenApiSchema } from "./openapi-v3";
export { OpenApi, OpenApiReference, OpenApiSchema } from "./openapi-v3";

export interface SwagData {
    openapi: string
    info: any
    servers: any[]
    paths: { [index:string] : Endpoint}
    components : {
        schemas : any
    }
}

export interface Endpoint {

}

const swagdata: OpenApi = {
    "openapi": "3.0.1",
    "info": {
      "title": "BouMatic Chemist App",
      "version": "1"
    },
    "servers": [
      {
        "url": "https://chemistapp.boumatic.qa.iotnxt.io"
      }
    ],
    "paths": {
      "/api/AppSettings/Get": {
        "get": {
          "tags": [
            "AppSettings"
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/AppSettings/GetSystemMode": {
        "get": {
          "tags": [
            "AppSettings"
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/AppSettings/SetSystemMode": {
        "post": {
          "tags": [
            "AppSettings"
          ],
          "parameters": [
            {
              "name": "mode",
              "in": "query",
              "schema": {
                "$ref": "#/components/schemas/AppMode"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/AppSettings/GetSiteVersion": {
        "get": {
          "tags": [
            "AppSettings"
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/AuditTrails/GetBatchAuditLog": {
        "post": {
          "tags": [
            "AuditTrails"
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "$ref": "#/components/schemas/AuditTrailSearchArgs"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/AuditTrailSearchArgs"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/AuditTrailSearchArgs"
                }
              },
              "application/*+json": {
                "schema": {
                  "$ref": "#/components/schemas/AuditTrailSearchArgs"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/BatchAuditLogListPagedResponse"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/BatchAuditLogListPagedResponse"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/BatchAuditLogListPagedResponse"
                  }
                }
              }
            }
          }
        }
      },
      "/api/AuditTrails/GetAuditTrails": {
        "get": {
          "tags": [
            "AuditTrails"
          ],
          "parameters": [
            {
              "name": "PageNumber",
              "in": "query",
              "schema": {
                "type": "integer",
                "format": "int32"
              }
            },
            {
              "name": "PageSize",
              "in": "query",
              "schema": {
                "type": "integer",
                "format": "int32"
              }
            },
            {
              "name": "WhereQuery",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            },
            {
              "name": "OrderByQuery",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            },
            {
              "name": "SelectQuery",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/AuditTrailResponseListPagedResponse"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/AuditTrailResponseListPagedResponse"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/AuditTrailResponseListPagedResponse"
                  }
                }
              }
            }
          }
        }
      },
      "/api/AuditTrails/GetAllUsers": {
        "get": {
          "tags": [
            "AuditTrails"
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/AuditTrails/GetLastUserToEditFormula": {
        "get": {
          "tags": [
            "AuditTrails"
          ],
          "parameters": [
            {
              "name": "formulaId",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/AuditTrails/GetBatchAuditReport": {
        "get": {
          "tags": [
            "AuditTrails"
          ],
          "parameters": [
            {
              "name": "startDate",
              "in": "query",
              "schema": {
                "type": "string",
                "format": "date-time"
              }
            },
            {
              "name": "endDate",
              "in": "query",
              "schema": {
                "type": "string",
                "format": "date-time"
              }
            },
            {
              "name": "referenceId",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/AuditTrails/UpsertAuditTrail": {
        "post": {
          "tags": [
            "AuditTrails"
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "$ref": "#/components/schemas/AuditTrail"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/AuditTrail"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/AuditTrail"
                }
              },
              "application/*+json": {
                "schema": {
                  "$ref": "#/components/schemas/AuditTrail"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/AuditTrails/ExportFormulaAuditTrail": {
        "post": {
          "tags": [
            "AuditTrails"
          ],
          "parameters": [
            {
              "name": "onlyUnique",
              "in": "query",
              "schema": {
                "type": "boolean",
                "default": false
              }
            }
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "$ref": "#/components/schemas/AuditTrailSearchArgs"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/AuditTrailSearchArgs"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/AuditTrailSearchArgs"
                }
              },
              "application/*+json": {
                "schema": {
                  "$ref": "#/components/schemas/AuditTrailSearchArgs"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/FormulaAuditTrailListPagedResponse"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/FormulaAuditTrailListPagedResponse"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/FormulaAuditTrailListPagedResponse"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Batches/GetBatches": {
        "get": {
          "tags": [
            "Batches"
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Batches/GetBatch": {
        "get": {
          "tags": [
            "Batches"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Batches/RevokeBatch": {
        "get": {
          "tags": [
            "Batches"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Batches/DeleteBatch": {
        "delete": {
          "tags": [
            "Batches"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Batches/GetBatchByLotNumber": {
        "get": {
          "tags": [
            "Batches"
          ],
          "parameters": [
            {
              "name": "lotNumber",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Batches/GetBatchByFormulaId": {
        "get": {
          "tags": [
            "Batches"
          ],
          "parameters": [
            {
              "name": "formulaId",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Batches/GetBatchByMixingTankId": {
        "get": {
          "tags": [
            "Batches"
          ],
          "parameters": [
            {
              "name": "mixingTankId",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Batches/UpsertBatch": {
        "post": {
          "tags": [
            "Batches"
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "$ref": "#/components/schemas/Batch"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Batch"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/Batch"
                }
              },
              "application/*+json": {
                "schema": {
                  "$ref": "#/components/schemas/Batch"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Batches/UpsertBatches": {
        "post": {
          "tags": [
            "Batches"
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Batch"
                  },
                  "nullable": true
                }
              },
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Batch"
                  },
                  "nullable": true
                }
              },
              "text/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Batch"
                  },
                  "nullable": true
                }
              },
              "application/*+json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Batch"
                  },
                  "nullable": true
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/BatchIngredients/GetBatchIngredients": {
        "get": {
          "tags": [
            "BatchIngredients"
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/BatchIngredients/GetBatchIngredient": {
        "get": {
          "tags": [
            "BatchIngredients"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/BatchIngredients/GetBatchIngredientByBatchId": {
        "get": {
          "tags": [
            "BatchIngredients"
          ],
          "parameters": [
            {
              "name": "batchId",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/BatchIngredients/GetBatchIngredientByChemicalId": {
        "get": {
          "tags": [
            "BatchIngredients"
          ],
          "parameters": [
            {
              "name": "chemicalId",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/BatchIngredients/UpsertBatchIngredient": {
        "post": {
          "tags": [
            "BatchIngredients"
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "$ref": "#/components/schemas/BatchIngredient"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/BatchIngredient"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/BatchIngredient"
                }
              },
              "application/*+json": {
                "schema": {
                  "$ref": "#/components/schemas/BatchIngredient"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/BatchIngredients/UpsertBatchIngredients": {
        "post": {
          "tags": [
            "BatchIngredients"
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "type": "array",
                  "items": { },
                  "nullable": true
                }
              },
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": { },
                  "nullable": true
                }
              },
              "text/json": {
                "schema": {
                  "type": "array",
                  "items": { },
                  "nullable": true
                }
              },
              "application/*+json": {
                "schema": {
                  "type": "array",
                  "items": { },
                  "nullable": true
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/BatchIngredients/DeleteBatchIngredient": {
        "delete": {
          "tags": [
            "BatchIngredients"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/CertificateOfAnalysis/GetAllCertificateOfAnalysis": {
        "get": {
          "tags": [
            "CertificateOfAnalysis"
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/CertificateOfAnalysis/GetCertificateOfAnalysis": {
        "get": {
          "tags": [
            "CertificateOfAnalysis"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/CertificateOfAnalysis/DeleteCertificateOfAnalysis": {
        "delete": {
          "tags": [
            "CertificateOfAnalysis"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/CertificateOfAnalysis/UpsertCertificateOfAnalysis": {
        "post": {
          "tags": [
            "CertificateOfAnalysis"
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "$ref": "#/components/schemas/CertificateOfAnalysis"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CertificateOfAnalysis"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/CertificateOfAnalysis"
                }
              },
              "application/*+json": {
                "schema": {
                  "$ref": "#/components/schemas/CertificateOfAnalysis"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/CertificateOfAnalysis/GetCertificateOfAnalysisByFormulaId": {
        "get": {
          "tags": [
            "CertificateOfAnalysis"
          ],
          "parameters": [
            {
              "name": "formulaId",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            },
            {
              "name": "lotNumber",
              "in": "query",
              "schema": {
                "type": "string",
                "default": "",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/CertificateOfAnalysis/GetCertificateOfAnalysisByBatchNo": {
        "get": {
          "tags": [
            "CertificateOfAnalysis"
          ],
          "parameters": [
            {
              "name": "lotNumber",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Chemicals/GetChemicals": {
        "get": {
          "tags": [
            "Chemicals"
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Chemicals/GetChemical": {
        "get": {
          "tags": [
            "Chemicals"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Chemicals/DeleteChemical": {
        "delete": {
          "tags": [
            "Chemicals"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Chemicals/UpsertChemical": {
        "post": {
          "tags": [
            "Chemicals"
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "$ref": "#/components/schemas/Chemical"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Chemical"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/Chemical"
                }
              },
              "application/*+json": {
                "schema": {
                  "$ref": "#/components/schemas/Chemical"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Configuration/CurrentConfiguration": {
        "post": {
          "tags": [
            "Configuration"
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/ICurrentConfigurationResponse"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Containers/GetContainers": {
        "get": {
          "tags": [
            "Containers"
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Containers/GetContainer": {
        "get": {
          "tags": [
            "Containers"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Containers/DeleteContainer": {
        "delete": {
          "tags": [
            "Containers"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Containers/UpsertContainer": {
        "post": {
          "tags": [
            "Containers"
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "$ref": "#/components/schemas/Container"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Container"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/Container"
                }
              },
              "application/*+json": {
                "schema": {
                  "$ref": "#/components/schemas/Container"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/DeviationDoc/DeviationDoc": {
        "get": {
          "tags": [
            "DeviationDoc"
          ],
          "responses": {
            "200": {
              "description": "Success"
            }
          }
        }
      },
      "/api/Formulas/GetFormulas": {
        "get": {
          "tags": [
            "Formulas"
          ],
          "parameters": [
            {
              "name": "includeInactiveFormulas",
              "in": "query",
              "schema": {
                "type": "boolean",
                "default": false
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Formulas/GetFormula": {
        "get": {
          "tags": [
            "Formulas"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Formulas/GetLastFormulaBasedOnSkuNumber": {
        "get": {
          "tags": [
            "Formulas"
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Formulas/UpsertFormula": {
        "post": {
          "tags": [
            "Formulas"
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "$ref": "#/components/schemas/Formula"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Formula"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/Formula"
                }
              },
              "application/*+json": {
                "schema": {
                  "$ref": "#/components/schemas/Formula"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/FormulaSteps/GetFormulaSteps": {
        "get": {
          "tags": [
            "FormulaSteps"
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/FormulaSteps/GetFormulaStepByFormulaId": {
        "get": {
          "tags": [
            "FormulaSteps"
          ],
          "parameters": [
            {
              "name": "formulaId",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/FormulaSteps/UpsertFormulaStep": {
        "post": {
          "tags": [
            "FormulaSteps"
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "$ref": "#/components/schemas/FormulaStep"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/FormulaStep"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/FormulaStep"
                }
              },
              "application/*+json": {
                "schema": {
                  "$ref": "#/components/schemas/FormulaStep"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/FormulaSteps/DeleteFormulaStep": {
        "delete": {
          "tags": [
            "FormulaSteps"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/FormulaSteps/GetFormulaStep": {
        "get": {
          "tags": [
            "FormulaSteps"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/FormulaSteps/ReOrderFormulaSteps": {
        "post": {
          "tags": [
            "FormulaSteps"
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/FormulaStep"
                  },
                  "nullable": true
                }
              },
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/FormulaStep"
                  },
                  "nullable": true
                }
              },
              "text/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/FormulaStep"
                  },
                  "nullable": true
                }
              },
              "application/*+json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/FormulaStep"
                  },
                  "nullable": true
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/metricsGraphs": {
        "get": {
          "tags": [
            "Graphs"
          ],
          "responses": {
            "200": {
              "description": "Success"
            }
          }
        }
      },
      "/metricsGraphs/updateData": {
        "get": {
          "tags": [
            "Graphs"
          ],
          "responses": {
            "200": {
              "description": "Success"
            }
          }
        }
      },
      "/api/Instructions/GetInstructions": {
        "get": {
          "tags": [
            "Instructions"
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Instructions/GetInstruction": {
        "get": {
          "tags": [
            "Instructions"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Instructions/DeleteInstruction": {
        "delete": {
          "tags": [
            "Instructions"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Instructions/UpsertInstruction": {
        "post": {
          "tags": [
            "Instructions"
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "$ref": "#/components/schemas/Instruction"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Instruction"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/Instruction"
                }
              },
              "application/*+json": {
                "schema": {
                  "$ref": "#/components/schemas/Instruction"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Instructions/GetInstructionByFormulaId": {
        "get": {
          "tags": [
            "Instructions"
          ],
          "parameters": [
            {
              "name": "formulaId",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Instructions/UploadInstructionsFile": {
        "post": {
          "tags": [
            "Instructions"
          ],
          "parameters": [
            {
              "name": "formulaId",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "requestBody": {
            "content": {
              "multipart/form-data": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "fileData": {
                      "type": "string",
                      "format": "binary",
                      "nullable": true
                    }
                  }
                },
                "encoding": {
                  "fileData": {
                    "style": "form"
                  }
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Instructions/ReOrderFormulaInstructions": {
        "post": {
          "tags": [
            "Instructions"
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Instruction"
                  },
                  "nullable": true
                }
              },
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Instruction"
                  },
                  "nullable": true
                }
              },
              "text/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Instruction"
                  },
                  "nullable": true
                }
              },
              "application/*+json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Instruction"
                  },
                  "nullable": true
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Methods/GetMethods": {
        "get": {
          "tags": [
            "Methods"
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Methods/GetMethod": {
        "get": {
          "tags": [
            "Methods"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Methods/UpsertMethod": {
        "post": {
          "tags": [
            "Methods"
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "$ref": "#/components/schemas/Method"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Method"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/Method"
                }
              },
              "application/*+json": {
                "schema": {
                  "$ref": "#/components/schemas/Method"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Methods/DeleteMethod": {
        "delete": {
          "tags": [
            "Methods"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/MixingTanks/GetMixingTanks": {
        "get": {
          "tags": [
            "MixingTanks"
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/MixingTanks/GetMixingTank": {
        "get": {
          "tags": [
            "MixingTanks"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/MixingTanks/DeleteMixingTank": {
        "delete": {
          "tags": [
            "MixingTanks"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/MixingTanks/UpsertMixingTank": {
        "post": {
          "tags": [
            "MixingTanks"
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "$ref": "#/components/schemas/MixingTank"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/MixingTank"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/MixingTank"
                }
              },
              "application/*+json": {
                "schema": {
                  "$ref": "#/components/schemas/MixingTank"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/ProductQuantity/GetProductQuantities": {
        "get": {
          "tags": [
            "ProductQuantity"
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/ProductQuantity/GetProductQuantity": {
        "get": {
          "tags": [
            "ProductQuantity"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/ProductQuantity/GetProductQuantityByBatchId": {
        "get": {
          "tags": [
            "ProductQuantity"
          ],
          "parameters": [
            {
              "name": "batchId",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/ProductQuantity/GetProductQuantityByProductId": {
        "get": {
          "tags": [
            "ProductQuantity"
          ],
          "parameters": [
            {
              "name": "productId",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/ProductQuantity/UpsertProductQuantity": {
        "post": {
          "tags": [
            "ProductQuantity"
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "$ref": "#/components/schemas/ProductQuantity"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ProductQuantity"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/ProductQuantity"
                }
              },
              "application/*+json": {
                "schema": {
                  "$ref": "#/components/schemas/ProductQuantity"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/ProductQuantity/UpsertProductQuantities": {
        "post": {
          "tags": [
            "ProductQuantity"
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/ProductQuantity"
                  },
                  "nullable": true
                }
              },
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/ProductQuantity"
                  },
                  "nullable": true
                }
              },
              "text/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/ProductQuantity"
                  },
                  "nullable": true
                }
              },
              "application/*+json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/ProductQuantity"
                  },
                  "nullable": true
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/ProductQuantity/DeleteProductQuantity": {
        "delete": {
          "tags": [
            "ProductQuantity"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Products/GetProducts": {
        "get": {
          "tags": [
            "Products"
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Products/GetProductByFormulaId": {
        "get": {
          "tags": [
            "Products"
          ],
          "parameters": [
            {
              "name": "formulaId",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Products/GetProduct": {
        "get": {
          "tags": [
            "Products"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Products/DeleteProduct": {
        "delete": {
          "tags": [
            "Products"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Products/UpsertProduct": {
        "post": {
          "tags": [
            "Products"
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "$ref": "#/components/schemas/Product"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Product"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/Product"
                }
              },
              "application/*+json": {
                "schema": {
                  "$ref": "#/components/schemas/Product"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Products/UpsertProducts": {
        "post": {
          "tags": [
            "Products"
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Product"
                  },
                  "nullable": true
                }
              },
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Product"
                  },
                  "nullable": true
                }
              },
              "text/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Product"
                  },
                  "nullable": true
                }
              },
              "application/*+json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Product"
                  },
                  "nullable": true
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/QualityControls/GetFormulaTolerances": {
        "get": {
          "tags": [
            "QualityControls"
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/QualityControls/GetFormulaToleranceByFormulaId": {
        "get": {
          "tags": [
            "QualityControls"
          ],
          "parameters": [
            {
              "name": "formulaId",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/QualityControls/UpsertFormulaTolerance": {
        "post": {
          "tags": [
            "QualityControls"
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "$ref": "#/components/schemas/FormulaTolerance"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/FormulaTolerance"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/FormulaTolerance"
                }
              },
              "application/*+json": {
                "schema": {
                  "$ref": "#/components/schemas/FormulaTolerance"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/QualityControls/DeleteFormulaTolerance": {
        "delete": {
          "tags": [
            "QualityControls"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/QualityControls/GetFormulaTolerance": {
        "get": {
          "tags": [
            "QualityControls"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Reports/GetFillTicketReport": {
        "get": {
          "tags": [
            "Reports"
          ],
          "parameters": [
            {
              "name": "formulaId",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Reports/GetBatchTicketReport": {
        "get": {
          "tags": [
            "Reports"
          ],
          "parameters": [
            {
              "name": "batchId",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Reports/GetFormulaReport": {
        "get": {
          "tags": [
            "Reports"
          ],
          "parameters": [
            {
              "name": "formulaId",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Reports/GetProductionLog": {
        "get": {
          "tags": [
            "Reports"
          ],
          "parameters": [
            {
              "name": "PageNumber",
              "in": "query",
              "schema": {
                "type": "integer",
                "format": "int32"
              }
            },
            {
              "name": "PageSize",
              "in": "query",
              "schema": {
                "type": "integer",
                "format": "int32"
              }
            },
            {
              "name": "WhereQuery",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            },
            {
              "name": "OrderByQuery",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            },
            {
              "name": "SelectQuery",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/ProductionLogListPagedResponse"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/ProductionLogListPagedResponse"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/ProductionLogListPagedResponse"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Users/GetAllUsers": {
        "get": {
          "tags": [
            "Users"
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Validations/GetValidations": {
        "get": {
          "tags": [
            "Validations"
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Validations/GetValidationByValidationSetId": {
        "get": {
          "tags": [
            "Validations"
          ],
          "parameters": [
            {
              "name": "validationSetId",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Validations/UpsertValidation": {
        "post": {
          "tags": [
            "Validations"
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "$ref": "#/components/schemas/Validation"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Validation"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/Validation"
                }
              },
              "application/*+json": {
                "schema": {
                  "$ref": "#/components/schemas/Validation"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Validations/DeleteValidation": {
        "delete": {
          "tags": [
            "Validations"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Validations/GetValidation": {
        "get": {
          "tags": [
            "Validations"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/Validations/GetValidationByFormulaToleranceId": {
        "get": {
          "tags": [
            "Validations"
          ],
          "parameters": [
            {
              "name": "formulaToleranceId",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/ValidationSets/GetValidationSets": {
        "get": {
          "tags": [
            "ValidationSets"
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/ValidationSets/GetValidationSetByBatchId": {
        "get": {
          "tags": [
            "ValidationSets"
          ],
          "parameters": [
            {
              "name": "batchId",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/ValidationSets/UpsertValidationSet": {
        "post": {
          "tags": [
            "ValidationSets"
          ],
          "requestBody": {
            "content": {
              "application/json-patch+json": {
                "schema": {
                  "$ref": "#/components/schemas/ValidationSet"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ValidationSet"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/ValidationSet"
                }
              },
              "application/*+json": {
                "schema": {
                  "$ref": "#/components/schemas/ValidationSet"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/ValidationSets/DeleteValidationSet": {
        "delete": {
          "tags": [
            "ValidationSets"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      },
      "/api/ValidationSets/GetValidationSet": {
        "get": {
          "tags": [
            "ValidationSets"
          ],
          "parameters": [
            {
              "name": "id",
              "in": "query",
              "schema": {
                "type": "string",
                "nullable": true
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Success",
              "content": {
                "text/plain": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                },
                "text/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CloudServiceActionResult"
                  }
                }
              }
            }
          }
        }
      }
    },
    "components": {
      "schemas": {
        "CloudServiceActionResult": {
          "type": "object",
          "properties": {
            "isSuccessful": {
              "type": "boolean"
            },
            "message": {
              "type": "string",
              "nullable": true
            },
            "data": {
              "nullable": true
            },
            "exceptionMessage": {
              "type": "string",
              "nullable": true
            },
            "count": {
              "type": "integer",
              "format": "int32"
            }
          },
          "additionalProperties": false
        },
        "AppMode": {
          "enum": [
            0,
            1
          ],
          "type": "integer",
          "format": "int32"
        },
        "AuditTrailSearchArgs": {
          "type": "object",
          "properties": {
            "formulaId": {
              "type": "string",
              "nullable": true
            },
            "chemicalId": {
              "type": "string",
              "nullable": true
            },
            "endDate": {
              "type": "string",
              "format": "date-time",
              "nullable": true
            },
            "mixingTankId": {
              "type": "string",
              "nullable": true
            },
            "startDate": {
              "type": "string",
              "format": "date-time",
              "nullable": true
            },
            "userId": {
              "type": "string",
              "nullable": true
            },
            "tableName": {
              "type": "string",
              "nullable": true
            },
            "pageNumber": {
              "type": "integer",
              "format": "int32"
            },
            "pageSize": {
              "type": "integer",
              "format": "int32"
            }
          },
          "additionalProperties": false
        },
        "BatchAuditLog": {
          "type": "object",
          "properties": {
            "weight": {
              "type": "number",
              "format": "double"
            },
            "lotNumber": {
              "type": "string",
              "nullable": true
            },
            "batchId": {
              "type": "string",
              "nullable": true
            },
            "batchState": {
              "type": "string",
              "nullable": true
            },
            "formulaName": {
              "type": "string",
              "nullable": true
            },
            "mixingTankNo": {
              "type": "integer",
              "format": "int32",
              "nullable": true
            },
            "createdDate": {
              "type": "string",
              "format": "date-time",
              "nullable": true
            },
            "createdBy": {
              "type": "string",
              "nullable": true
            }
          },
          "additionalProperties": false
        },
        "BatchAuditLogListPagedResponse": {
          "type": "object",
          "properties": {
            "data": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/BatchAuditLog"
              },
              "nullable": true
            },
            "isSuccessful": {
              "type": "boolean"
            },
            "message": {
              "type": "string",
              "nullable": true
            },
            "exceptionMessage": {
              "type": "string",
              "nullable": true
            },
            "pageNumber": {
              "type": "integer",
              "format": "int32"
            },
            "pageSize": {
              "type": "integer",
              "format": "int32"
            },
            "totalPages": {
              "type": "integer",
              "format": "int32"
            },
            "totalRecords": {
              "type": "integer",
              "format": "int32"
            }
          },
          "additionalProperties": false
        },
        "AuditTrailResponse": {
          "type": "object",
          "properties": {
            "actionedBy": {
              "type": "string",
              "nullable": true
            },
            "actionDate": {
              "type": "string",
              "format": "date-time",
              "nullable": true
            },
            "action": {
              "type": "string",
              "nullable": true
            },
            "referenceId": {
              "type": "string",
              "nullable": true
            },
            "description": {
              "type": "string",
              "nullable": true
            },
            "propertyName": {
              "type": "string",
              "nullable": true
            },
            "oldValue": {
              "type": "string",
              "nullable": true
            },
            "newValue": {
              "type": "string",
              "nullable": true
            }
          },
          "additionalProperties": false
        },
        "AuditTrailResponseListPagedResponse": {
          "type": "object",
          "properties": {
            "data": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/AuditTrailResponse"
              },
              "nullable": true
            },
            "isSuccessful": {
              "type": "boolean"
            },
            "message": {
              "type": "string",
              "nullable": true
            },
            "exceptionMessage": {
              "type": "string",
              "nullable": true
            },
            "pageNumber": {
              "type": "integer",
              "format": "int32"
            },
            "pageSize": {
              "type": "integer",
              "format": "int32"
            },
            "totalPages": {
              "type": "integer",
              "format": "int32"
            },
            "totalRecords": {
              "type": "integer",
              "format": "int32"
            }
          },
          "additionalProperties": false
        },
        "CacheItemState": {
          "enum": [
            0,
            1,
            2,
            3
          ],
          "type": "integer",
          "format": "int32"
        },
        "AuditAction": {
          "enum": [
            1,
            2,
            3
          ],
          "type": "integer",
          "format": "int32"
        },
        "AuditTrail": {
          "type": "object",
          "properties": {
            "userId": {
              "type": "string",
              "nullable": true
            },
            "action": {
              "$ref": "#/components/schemas/AuditAction"
            },
            "referenceId": {
              "type": "string",
              "nullable": true
            },
            "description": {
              "type": "string",
              "nullable": true
            },
            "propertyName": {
              "type": "string",
              "nullable": true
            },
            "oldValue": {
              "type": "string",
              "nullable": true
            },
            "newValue": {
              "type": "string",
              "nullable": true
            },
            "tableName": {
              "type": "string",
              "nullable": true
            }
          },
          "additionalProperties": false
        },
        "FormulaAuditTrail": {
          "type": "object",
          "properties": {
            "createdBy": {
              "type": "string",
              "nullable": true
            },
            "createdDate": {
              "type": "string",
              "format": "date-time",
              "nullable": true
            },
            "action": {
              "$ref": "#/components/schemas/AuditAction"
            },
            "formulaId": {
              "type": "string",
              "nullable": true
            },
            "formulaName": {
              "type": "string",
              "nullable": true
            },
            "description": {
              "type": "string",
              "nullable": true
            },
            "propertyName": {
              "type": "string",
              "nullable": true
            },
            "oldValue": {
              "type": "string",
              "nullable": true
            },
            "newValue": {
              "type": "string",
              "nullable": true
            },
            "tableName": {
              "type": "string",
              "nullable": true
            }
          },
          "additionalProperties": false
        },
        "FormulaAuditTrailListPagedResponse": {
          "type": "object",
          "properties": {
            "data": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/FormulaAuditTrail"
              },
              "nullable": true
            },
            "isSuccessful": {
              "type": "boolean"
            },
            "message": {
              "type": "string",
              "nullable": true
            },
            "exceptionMessage": {
              "type": "string",
              "nullable": true
            },
            "pageNumber": {
              "type": "integer",
              "format": "int32"
            },
            "pageSize": {
              "type": "integer",
              "format": "int32"
            },
            "totalPages": {
              "type": "integer",
              "format": "int32"
            },
            "totalRecords": {
              "type": "integer",
              "format": "int32"
            }
          },
          "additionalProperties": false
        },
        "BatchState": {
          "enum": [
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9
          ],
          "type": "integer",
          "format": "int32"
        },
        "Batch": {
          "type": "object",
          "properties": {
            "timeStamp": {
              "type": "string",
              "format": "date-time"
            },
            "lotNumber": {
              "type": "string",
              "nullable": true
            },
            "mixingTankId": {
              "type": "string",
              "nullable": true
            },
            "formulaId": {
              "type": "string",
              "nullable": true
            },
            "formulaMultiplier": {
              "type": "number",
              "format": "double"
            },
            "weight": {
              "type": "number",
              "format": "double"
            },
            "batchVolume": {
              "type": "number",
              "format": "double"
            },
            "batchState": {
              "$ref": "#/components/schemas/BatchState"
            },
            "queuePlace": {
              "type": "integer",
              "format": "int32"
            },
            "nextPlcStep": {
              "type": "integer",
              "format": "int32"
            },
            "fillInstructs": {
              "type": "string",
              "nullable": true
            },
            "comments": {
              "type": "string",
              "nullable": true
            }
          },
          "additionalProperties": false
        },
        "StepStatus": {
          "enum": [
            0,
            1,
            2
          ],
          "type": "integer",
          "format": "int32"
        },
        "BatchIngredient": {
          "type": "object",
          "properties": {
            "batchId": {
              "type": "string",
              "nullable": true
            },
            "chemicalId": {
              "type": "string",
              "nullable": true
            },
            "stepNumber": {
              "type": "integer",
              "format": "int32"
            },
            "lotNumber": {
              "type": "string",
              "nullable": true
            },
            "weight": {
              "type": "number",
              "format": "double"
            },
            "adjustmentWeight": {
              "type": "number",
              "format": "double"
            },
            "weightAdded": {
              "type": "integer",
              "format": "int32"
            },
            "agitatorRunTime": {
              "type": "number",
              "format": "double"
            },
            "agitatorFreq": {
              "type": "number",
              "format": "double"
            },
            "stepStatus": {
              "$ref": "#/components/schemas/StepStatus"
            }
          },
          "additionalProperties": false
        },
        "TestDescription": {
          "type": "object",
          "properties": {
            "formulaId": {
              "type": "string",
              "nullable": true
            },
            "methodId": {
              "type": "string",
              "nullable": true
            },
            "minimum": {
              "type": "number",
              "format": "double"
            },
            "maximum": {
              "type": "number",
              "format": "double"
            },
            "units": {
              "type": "string",
              "nullable": true
            },
            "active": {
              "type": "boolean"
            },
            "measuredValue": {
              "type": "number",
              "format": "double"
            },
            "method": {
              "type": "string",
              "nullable": true
            },
            "isOk": {
              "type": "boolean"
            }
          },
          "additionalProperties": false
        },
        "CertificateOfAnalysis": {
          "type": "object",
          "properties": {
            "lotNumber": {
              "type": "string",
              "nullable": true
            },
            "mixingTankId": {
              "type": "string",
              "nullable": true
            },
            "formulaId": {
              "type": "string",
              "nullable": true
            },
            "formulaName": {
              "type": "string",
              "nullable": true
            },
            "userId": {
              "type": "string",
              "nullable": true
            },
            "firstName": {
              "type": "string",
              "nullable": true
            },
            "lastSurname": {
              "type": "string",
              "nullable": true
            },
            "testingDate": {
              "type": "string",
              "format": "date-time"
            },
            "manufactureDate": {
              "type": "string",
              "format": "date-time"
            },
            "testDescriptions": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/TestDescription"
              },
              "nullable": true
            },
            "comments": {
              "type": "string",
              "nullable": true
            }
          },
          "additionalProperties": false
        },
        "Chemical": {
          "type": "object",
          "properties": {
            "skuNumber": {
              "type": "integer",
              "format": "int32"
            },
            "name": {
              "type": "string",
              "nullable": true
            },
            "alternativeName": {
              "type": "string",
              "nullable": true
            },
            "plcId": {
              "type": "string",
              "nullable": true
            },
            "active": {
              "type": "boolean"
            },
            "product": {
              "type": "string",
              "nullable": true
            }
          },
          "additionalProperties": false
        },
        "ICurrentConfigurationResponse": {
          "type": "object",
          "properties": {
            "configuration": {
              "nullable": true
            }
          },
          "additionalProperties": false
        },
        "Container": {
          "type": "object",
          "properties": {
            "skuNumber": {
              "type": "integer",
              "format": "int32"
            },
            "name": {
              "type": "string",
              "nullable": true
            },
            "active": {
              "type": "boolean"
            },
            "capacityVolume": {
              "type": "number",
              "format": "double"
            }
          },
          "additionalProperties": false
        },
        "FormulaState": {
          "enum": [
            0,
            1,
            2,
            3
          ],
          "type": "integer",
          "format": "int32"
        },
        "Formula": {
          "type": "object",
          "properties": {
            "skuNumber": {
              "type": "integer",
              "format": "int32"
            },
            "plcId": {
              "type": "integer",
              "format": "int32"
            },
            "name": {
              "type": "string",
              "nullable": true
            },
            "compatabilityGroup": {
              "type": "integer",
              "format": "int32"
            },
            "maxVolumePct": {
              "type": "number",
              "format": "double"
            },
            "specificGravity": {
              "type": "number",
              "format": "double"
            },
            "defaultVolume": {
              "type": "number",
              "format": "double"
            },
            "shelfLife": {
              "type": "integer",
              "format": "int32"
            },
            "active": {
              "type": "boolean"
            },
            "revision": {
              "type": "string",
              "nullable": true
            },
            "canAction": {
              "type": "boolean"
            },
            "specialInstruction": {
              "type": "string",
              "nullable": true
            },
            "state": {
              "$ref": "#/components/schemas/FormulaState"
            }
          },
          "additionalProperties": false
        },
        "FormulaStep": {
          "type": "object",
          "properties": {
            "chemicalId": {
              "type": "string",
              "nullable": true
            },
            "formulaId": {
              "type": "string",
              "nullable": true
            },
            "stepNumber": {
              "type": "integer",
              "format": "int32"
            },
            "percentage": {
              "type": "number",
              "format": "double"
            },
            "agitatorRunTime": {
              "type": "integer",
              "format": "int32"
            },
            "agitatorFreq": {
              "type": "integer",
              "format": "int32"
            },
            "active": {
              "type": "boolean"
            }
          },
          "additionalProperties": false
        },
        "Instruction": {
          "type": "object",
          "properties": {
            "formulaId": {
              "type": "string",
              "nullable": true
            },
            "stepNumber": {
              "type": "integer",
              "format": "int32"
            },
            "procedure": {
              "type": "string",
              "nullable": true
            }
          },
          "additionalProperties": false
        },
        "Method": {
          "type": "object",
          "properties": {
            "name": {
              "type": "string",
              "nullable": true
            },
            "active": {
              "type": "boolean"
            }
          },
          "additionalProperties": false
        },
        "MixingTank": {
          "type": "object",
          "properties": {
            "batchId": {
              "type": "string",
              "nullable": true
            },
            "compatabilityGroup": {
              "type": "integer",
              "format": "int32"
            },
            "tankNo": {
              "type": "integer",
              "format": "int32"
            },
            "capacityVolume": {
              "type": "number",
              "format": "double"
            },
            "active": {
              "type": "boolean"
            }
          },
          "additionalProperties": false
        },
        "ProductQuantity": {
          "type": "object",
          "properties": {
            "batchId": {
              "type": "string",
              "nullable": true
            },
            "productId": {
              "type": "string",
              "nullable": true
            },
            "quantity": {
              "type": "integer",
              "format": "int32"
            }
          },
          "additionalProperties": false
        },
        "Product": {
          "type": "object",
          "properties": {
            "formulaId": {
              "type": "string",
              "nullable": true
            },
            "containerId": {
              "type": "string",
              "nullable": true
            },
            "skuNumber": {
              "type": "integer",
              "format": "int32"
            },
            "active": {
              "type": "boolean"
            },
            "queuePlace": {
              "type": "integer",
              "format": "int32"
            }
          },
          "additionalProperties": false
        },
        "FormulaTolerance": {
          "type": "object",
          "properties": {
            "formulaId": {
              "type": "string",
              "nullable": true
            },
            "methodId": {
              "type": "string",
              "nullable": true
            },
            "minimum": {
              "type": "number",
              "format": "double"
            },
            "maximum": {
              "type": "number",
              "format": "double"
            },
            "units": {
              "type": "string",
              "nullable": true
            },
            "active": {
              "type": "boolean"
            }
          },
          "additionalProperties": false
        },
        "ProductionLog": {
          "type": "object",
          "properties": {
            "id": {
              "type": "string",
              "nullable": true
            },
            "formulaName": {
              "type": "string",
              "nullable": true
            },
            "formulaSku": {
              "type": "string",
              "nullable": true
            },
            "batchSizeInGallons": {
              "type": "number",
              "format": "double"
            },
            "batchSizeInPounds": {
              "type": "number",
              "format": "double"
            },
            "dateMade": {
              "type": "string",
              "format": "date-time",
              "nullable": true
            },
            "lotNumber": {
              "type": "string",
              "nullable": true
            },
            "tankNo": {
              "type": "integer",
              "format": "int32"
            },
            "yield": {
              "type": "number",
              "format": "double"
            }
          },
          "additionalProperties": false
        },
        "ProductionLogListPagedResponse": {
          "type": "object",
          "properties": {
            "data": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/ProductionLog"
              },
              "nullable": true
            },
            "isSuccessful": {
              "type": "boolean"
            },
            "message": {
              "type": "string",
              "nullable": true
            },
            "exceptionMessage": {
              "type": "string",
              "nullable": true
            },
            "pageNumber": {
              "type": "integer",
              "format": "int32"
            },
            "pageSize": {
              "type": "integer",
              "format": "int32"
            },
            "totalPages": {
              "type": "integer",
              "format": "int32"
            },
            "totalRecords": {
              "type": "integer",
              "format": "int32"
            }
          },
          "additionalProperties": false
        },
        "Validation": {
          "type": "object",
          "properties": {
            "validationSetId": {
              "type": "string",
              "nullable": true
            },
            "formulaToleranceId": {
              "type": "string",
              "nullable": true
            },
            "measurement": {
              "type": "number",
              "format": "double"
            }
          },
          "additionalProperties": false
        },
        "ValidationSet": {
          "type": "object",
          "properties": {
            "batchId": {
              "type": "string",
              "nullable": true
            }
          },
          "additionalProperties": false
        }
      },
      "securitySchemes": {
        "Bearer": {
          "type": "oauth2",
          "flows": {
            "implicit": {
              "authorizationUrl": "https://boumatic.commander.io/auth/connect/authorize",
              "scopes": {
                "role": "Include authentication roles",
                "permissions": "Include authentication permissions",
                "api": "Include API access"
              }
            }
          }
        }
      }
    },
    "security": [
      {
        "Bearer": [ ]
      }
    ]
  }