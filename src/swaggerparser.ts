
import fs from 'fs';
import fetch from 'isomorphic-fetch'
import { IProcessArgs, OpenApi, OpenApiReference, OpenApiSchema } from './interfaces';
import { OpenApiParameter, OpenApiTag } from './interfaces/openapi-v3';
import * as _ from 'lodash';

interface State {
    swagdata?: any
}


export class SwaggerParser {
    state: State = {}

    // componentDidMount = async () => {
    //     const swagdata: OpenApi = file as OpenApi; //await fetch('https://gijima-api.qa.iotnxt.io/swagger/v1/swagger.json').then(res => res.json())
    //     console.log(swagdata);
    //     // this.setState({ swagdata });
    // }

    schemaTypeToTS = (datain: OpenApiSchema | OpenApiReference | any): string => {
        let result = '';

        if (datain["$ref"]) {
            let ref = datain as OpenApiReference;
            if (ref.$ref) result += ref.$ref.split('#/components/schemas/')[1];
            return result;
        }

        let x = datain as OpenApiSchema;

        switch (x.type) {
            case 'array': {
                if (x.items) {
                    let z: OpenApiSchema = x.items as OpenApiSchema;
                    result += this.schemaTypeToTS(z);
                }
                result += `[]`;
                break;
            }
            case 'integer': {
                if (x.enum) {
                    // result += '{\n'
                    // result += x.enum.map(e => `\t${e},\n`).join('')
                    result += x.enum.join(' | ')
                    // result += '}'
                } else {
                    result += `number`
                }
                break;
            }

            case 'string': {
                if (x.enum) {
                    result += x.enum.map(e => `"${e}"`).join(' | ')
                } else {
                    result += `${x.type}`
                }
                break;
            }

            case 'string array': {

                result += `string[]`;
                break;
            }
            case undefined: {
                result += `any`
                break;
            }
            default:
                result += `${x.type}`
                break;
        }
        return result;
    }

    componentSchemaToTSInterface = (c: [string, OpenApiSchema | OpenApiReference]) => {
        const name = c[0]
        const schema = c[1] as any;

        if (schema.type === 'object' && schema.oneOf) {
            let result = `export type ${c[0]} = `;

            let typesList = Object.entries(schema.oneOf).map(p => {
                const prop = p as [string, OpenApiSchema];
                return this.schemaTypeToTS(prop[1]);
            })

            result += typesList.join(' | ')

            result += `; \n\n`;
            return result;
        }

        if (schema.type === 'object' || schema.properties) {
            const schemaObject: OpenApiSchema = schema as OpenApiSchema;
            let result = `export interface ${c[0]} {`;

            if (schemaObject.properties) result += `\n`;
            if (schemaObject.properties) Object.entries(schemaObject.properties).map(p => {
                const prop = p as [string, OpenApiSchema];
                result += `\t${prop[0]}`
                if (prop[1].nullable) result += '?';
                result += ': ';
                result += this.schemaTypeToTS(prop[1]);
                result += '\n';
            })

            result += `}; \n\n`;
            return result;
        }

        if (schema.type === 'integer' && schema.enum) {
            return `export type ${c[0]} = ${this.schemaTypeToTS(schema)};\n\n`
        }

        if (schema.type === 'string' && schema.enum) {
            return `export type ${c[0]} = ${this.schemaTypeToTS(schema)};\n\n`
        }

        return `ERR export interface ${c[0]} ${schema.type} \n\n`
    }

    generateInterfaces = (swagdata: OpenApi) => {
        if (!swagdata?.components?.schemas) throw 'missing components.schemas'

        return Object.entries(swagdata.components.schemas).map((c) => { return this.componentSchemaToTSInterface(c) }).join('');
    }

    generateParameters = (parameters?: OpenApiParameter[]): string => {
        if (!parameters) return '';

        let tsparams = 'params: { ';

        parameters.forEach(p => {
            let type = 'unknown';

            if (p.schema) {
                let a = p.schema as any;
                type = this.schemaTypeToTS(p.schema);

            }

            // if param name has special characters wrap it as a string.
            if (p.name.indexOf('-') >= 0) p.name = `"${p.name}"`

            tsparams += `${p.name}: ${type}, `;
        })
        tsparams += '}';
        return tsparams;
    }
    generateUrlParameters = (parameters: OpenApiParameter[]): string => {
        let parameterUrl = `?`

        return '?'
    }



    generateClassMethodBindings = (swagdata: OpenApi): string => {
        let methodBinding = {};

        let output = '';

        Object.keys(swagdata.paths).forEach(path => {
            Object.keys(swagdata.paths[path]).forEach(method => {

                const pathdotnotation = path.split('/').join('.');
                _.set(methodBinding, pathdotnotation.slice(1), `this.${this.generateMethodName(method, path)}`);
            })
        });

        // console.log(Object.keys(methodBinding))


        Object.keys(methodBinding).forEach(name => {
            // @ts-ignore
            output += `\n\t${this.escapeString(name)} = ${JSON.stringify(methodBinding[name], null, 2).split('"').join('').split('\n').join('\n\t')}\n\n`;
        })


        return output;
    }

    escapeString = (path: string): string => {
        return path.split('-').join('_')
            .split('/').join('')
            .split('{').join('_')
            .split(':').join('_')
            .split('}').join('_');
    }

    generateMethodName = (method: string, path: string): string => {
        return `rest_${method}_${this.escapeString(path)}`;
    }

    className = (swagdata: OpenApi) => {
        return `API${swagdata.info.title.split(' ').join('')}`;
    }

    generateAPI = (swagdata: OpenApi) => {
        let interfaceList = (swagdata.components?.schemas) ? Object.keys(swagdata.components.schemas) : [];

        let APIClassName = swagdata.info.title.split(' ').join('');

        let apiTS = `// Title: ${swagdata.info.title}
// Auto Generated API using OpenSwag on ${new Date().toISOString()}
// Servers: ${swagdata.servers?.map(s => s.url).join('\n// ')}

import {\n\t${interfaceList.join(',\n\t')}\n} from './interfaces';

export class ${this.className(swagdata)} {
    authorization = '';
    baseUrl = '';

`;

        // generate private rest functions   


        Object.keys(swagdata.paths).forEach(path => {
            Object.keys(swagdata.paths[path]).forEach(method => {

                console.log(`${path} ${method}`);

                // @ts-ignore
                let z = swagdata.paths[path][method];

                if (!z) {
                    console.log('missing data...')
                    return;
                }

                let responseType = 'unknown';

                if (!z.responses) return;
                if (!z.responses['200']) return;

                let a = z.responses['200'].content;

                // @ts-ignore
                let parameters: OpenApiParameter[] = z.parameters;

                if (a) {
                    let contenttype = Object.keys(a)[0];
                    if (a[contenttype].schema?.$ref) responseType = a[contenttype].schema.$ref.split("#/components/schemas/")[1];
                }



                // if (swagdata.paths[path][method].responses['200'].content[0].schema['$ref'] != undefined) {
                //     // @ts-ignore
                //     responseType = swagdata.paths[path][method].responses['200'].content['application/json'].schema['$ref'];
                // }


                let pathprocessed = path;
                // @ts-ignore

                if (parameters) {
                    // @ts-ignore
                    pathprocessed += '?${encodeQueryData(params)}';
                }



                apiTS += `
    private ${this.generateMethodName(method, path)} = async (${this.generateParameters(parameters)}): Promise<${responseType}> => {
        return fetch(\`\${this.baseUrl}${pathprocessed}\`, {
            method: '${method}',
            headers: {
                authorization : this.authorization,
            },${(method === 'post') ? `
            body : JSON.stringify({}),` : ''}            
        }).then((res) => res.json());
    };
`;


            })
        });

        apiTS += this.generateClassMethodBindings(swagdata);

        apiTS += '}\n';

        // HELPER FUNCTIONS
        apiTS += `
export function encodeQueryData(data: Record<string, unknown>): string {
    const ret = [];
    for (const d in data) ret.push(\`\${encodeURIComponent(d)}=\${encodeURIComponent(data[d] as string)}\`);
    return ret.join('&');
}`;

        return apiTS;
    }

    /// GENERATING TESTS:

    generateTests = (swagdata: OpenApi): string => {
        let output = '';

        output += `// Generated test file (mocha)

import { ${this.className(swagdata)} } from './api';

describe("${this.className(swagdata)}", () => {
    const yourapi = new ${this.className(swagdata)}();
`;

        let last = {
            depth: 0,
            name: ''
        }

        Object.keys(swagdata.paths).forEach(path => {
            Object.keys(swagdata.paths[path]).forEach(method => {


                output += `
    it("${path}", async () => {
        const response = await yourapi.${this.escapeString(path.split("/").join(".").slice(1))}();
    });\n`

            })
        });

        output += `})`;

        return output;
    }

    async process(options: IProcessArgs) {
        let swagdata: any = {};

        if (options.url) {
            console.log(`downloading ${options.url}`)
            swagdata = await fetch(options.url).then(res => res.json())
        }

        if (options.file) {
            console.log(`opening ${options.file}`);
            swagdata = await fs.promises.readFile(options.file).then(data => JSON.parse(data.toString()));
        }

        console.log(`writing output/swagger.json`)
        await fs.promises.writeFile('swagger.json', JSON.stringify(swagdata, null, 2));

        if (!swagdata.components) throw 'missing components from swagger.json';
        if (!swagdata.components.schemas) throw 'missing components.schemas from swagger.json';

        // GENERATE INTERFACE
        const interfaces = this.generateInterfaces(swagdata);
        console.log('writing interfaces.ts');
        await fs.promises.writeFile('interfaces.ts', interfaces);

        // GENERATE API
        const generatedapi = this.generateAPI(swagdata);
        console.log('writing generatedapi.ts');
        await fs.promises.writeFile('api.ts', generatedapi);

        // // GENERATE TESTS
        // const generatedtest = this.generateTests(swagdata);
        // console.log('writing api.spec.ts');
        // await fs.promises.writeFile('api.spec.ts',generatedtest);
    }

}
