Auto generate typescript class interface in one step from a swagger openapi .json file.

[![pipeline status](https://gitlab.com/rvdende/openswag/badges/main/pipeline.svg)](https://gitlab.com/rvdende/openswag/-/commits/main)



## Run it now:
```
npx openswag https://hostname.com/swagger/v1/swagger.json
```

or pass in a local file:

```
npx openswag swagger.json
```

check for output files: interfaces.ts, api.ts, swagger.json


# DEV

`nodemon src/index.ts testfile.json` 